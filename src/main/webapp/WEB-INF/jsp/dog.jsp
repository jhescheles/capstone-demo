<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
<title>Dog</title>
</head>
<body>
<div class="jumbotron"><h1>${dogs}</h1></div>
<form:form commandName="formDog">
<div class="well"><div class="input-group">
	<span class="input-group-addon"><form:label path="name">Cat Name</form:label></span>
	<form:input path="name" class="form-control" placeholder="Name" />
</div>	
<div class="input-group">
	<span class="input-group-addon"><form:label path="age">Cat Age</form:label></span>
	<form:input path="age" class="form-control" placeholder="Age" />
</div>	
<div class="input-group">
	<span class="input-group-addon"><form:label path="color">Cat Color</form:label></span>
	<form:input path="color" class="form-control" placeholder="Color" />
</div>
<br/>
<br/>
<input type="submit" class="btn btn-primary" value="Chase Cat" />
</div>

	
	
	
	
</form:form>

<script src="../bootstrap/js/jquery.min.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
</body>
</html>