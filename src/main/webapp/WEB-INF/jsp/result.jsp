<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
The cat's name is ${ cat.name }.<br/>
The cat is ${cat.age} years old.<br/>
The cat is colored ${cat.color}.<br/>
<div class="alert alert-danger" >${ cat.name } the ${cat.color} cat is being chased by a Corgi. <br/></div>


<script src="../bootstrap/js/jquery.min.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
</body>
</html>