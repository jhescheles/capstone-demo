package com.metlife.dpa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.metlife.dpa.model.Cat;
import com.metlife.dpa.model.Greeting;

@Controller
public class DogController {


	

	@RequestMapping(value="/dog", method=RequestMethod.GET)
    public String greetingForm(Model model) {
		Cat cat = new Cat();
		cat.setName("");
		cat.setAge("");
		cat.setColor("");
        model.addAttribute("formDog", cat);
        
        model.addAttribute("dogs", "Corgis are cool and short! They Chase cats all the time.");
        return "dog";
    }
	@RequestMapping(value="/dog", method=RequestMethod.POST)
    public String dogSubmit(@ModelAttribute Cat cat, Model model) {
    	System.out.println(cat.getName());
        model.addAttribute("name", cat);
        model.addAttribute("age", cat);
        model.addAttribute("color", cat);
        return "result";
    }
}